!     ==================================================================
      program main
!     ==--------------------------------------------------------------==
!     == read the initial and final input files and generate images   ==
!     == by the linear interpolation                                  ==
!     ==--------------------------------------------------------------==
      implicit none
      integer                      :: katm,natm,natm2,ktyp,ntyp,&
                                      ncord,ninv,&
                                      iline,nline,idummy
      integer                      :: ii,jj,kk,iatm,it
      integer, allocatable         :: iwei(:),imdtyp(:),ityp(:)
      real(kind=8)                 :: pi,bohr
      real(kind=8)                 :: aaa,bbb,ccc,alpha,beta,gamma,&
                                      dummy,tmp
      real(kind=8)                 :: altv(3,3)
      real(kind=8), allocatable    :: cpsi(:,:),posi(:,:)
      real(kind=8), allocatable    :: cpsf(:,:),posf(:,:)
      real(kind=8), allocatable    :: cps(:,:)
      character(len=2),allocatable :: type(:)
      integer                      :: nfinp,nfout,nudged,ios
      logical                      :: fexist
      character(len=128)           :: filen,string
      character(len=2)             :: atmnm
      integer                      :: nfgeom,inscan,ierr
      integer                      :: ktyt,katt1,katt2
      character(len=20)            :: sec
! ...
      integer :: nimage,iimage,ndiv
      real(kind=8) :: fac,fac1,fac2,spring
! ...
      integer iarg,iargc,narg,marg
      character(len=128) :: arg
      character(len=2)   :: c2
      character(len=128) :: nfini,nffin,tmpfile
! ... code
      character(len=128) :: codnam='preneb'
!     ==--------------------------------------------------------------==
      nfinp=10
      nfout=20 
      nudged=30
      nfgeom=40
!     ==--------------------------------------------------------------==
      pi=4.d0*atan(1.d0)
      bohr=0.529177d0
!     ==--------------------------------------------------------------==
      narg=iargc()
      if(narg.ne.3)then
        write(*,'(a,a,a)')&
        ' Usage: ',trim(codnam),&
     &  ' -num_of_division [initial_input] [final_input]'
        stop
      endif 
      call getarg(1,arg)
      arg=adjustl(arg)
      if(arg(1:1).ne.'-')then
        write(*,'(a,a,a)')&
        ' Usage: ',trim(codnam),&
     &  ' -num_of_division [initial_input] [final_input]'
        stop
      else
        arg=arg(2:)
        read(arg,*)ndiv
      endif 
      call getarg(2,arg)
      nfini=adjustl(arg)
      call getarg(3,arg)
      nffin=adjustl(arg)
!     ==--------------------------------------------------------------==
! ... reading the initial input file
      filen=nfini
      inquire(file=filen,exist=fexist)
      if(.not.fexist)then
        write(*,'(a,a,a)')'*** ERROR: ',trim(filen),' does not exist.'
        stop
      endif
      open(nfinp,file=filen,iostat=ios,status='old')
      read(nfinp,*) idummy
      read(nfinp,*) dummy, dummy, ntyp, natm,natm2
      ktyp=ntyp
      katm=natm2
      allocate(cpsi(3,katm),posi(3,katm))
      allocate(cps(3,katm))
      allocate(iwei(katm),imdtyp(katm),ityp(katm))
      allocate(type(ktyp))
      read(nfinp,*) idummy, idummy
      read(nfinp,'(a)')string
      string=adjustl(string)
      if(string(1:9).eq.'Cartesian'.or.&
         string(1:9).eq.'cartesian'.or.&
         string(1:9).eq.'CARTESIAN'     )then
        do jj=1,3
          read(nfinp,*)(altv(ii,jj),ii=1,3)
        enddo        
      else
!       backspace(nfinp)
!       read(nfinp,*)aaa,bbb,ccc,alpha,beta,gamma
        read(string,*)aaa,bbb,ccc,alpha,beta,gamma
        altv(:,:)=0.d0
        alpha=alpha/180.d0*pi
        beta =beta /180.d0*pi
        gamma=gamma/180.d0*pi
        altv(1,1)=aaa
        altv(1,2)=bbb*cos(gamma)
        altv(2,2)=bbb*sin(gamma)
        tmp=(cos(alpha)-cos(beta)*cos(gamma))/sin(gamma)
        altv(1,3)=ccc*cos(beta)
        altv(2,3)=ccc*tmp
        altv(3,3)=sqrt(ccc**2-altv(1,3)**2+altv(2,3)**2)
      endif
      read(nfinp,'(a)')string
      string=adjustl(string)
      if(string(1:6).eq.'dipole'.or. &
         string(1:6).eq.'DIPOLE'.or. &
         string(1:6).eq.'Dipole')then
        read(nfinp,*) idummy, idummy, idummy
      else
        read(string,*) idummy, idummy, idummy
      endif
      read(nfinp,*) ncord,ninv
      if(ncord.eq.1)then
        do iatm=1,katm
          read(nfinp,*)(cpsi(ii,iatm),ii=1,3),&
                       iwei(iatm),imdtyp(iatm),ityp(iatm)
        enddo
      elseif(ncord.eq.0)then
        do iatm=1,katm
          read(nfinp,*)(posi(ii,iatm),ii=1,3),&
                       iwei(iatm),imdtyp(iatm),ityp(iatm)
        enddo
        do iatm=1,katm
          cpsi(1,iatm)= altv(1,1)*posi(1,iatm)&
                      +altv(1,2)*posi(2,iatm)&
                      +altv(1,3)*posi(3,iatm)
          cpsi(2,iatm)= altv(2,1)*posi(1,iatm)&
                      +altv(2,2)*posi(2,iatm)&
                      +altv(2,3)*posi(3,iatm)
          cpsi(3,iatm)= altv(3,1)*posi(1,iatm)&
                      +altv(3,2)*posi(2,iatm)&
                      +altv(3,3)*posi(3,iatm)
        enddo
      endif 
      close(nfinp)
!     ==--------------------------------------------------------------==
! ... reading the final image
      filen=nffin
      inquire(file=filen,exist=fexist)
      if(.not.fexist)then
        write(*,'(a,a,a)')'*** ERROR: ',trim(filen),' does not exist.'
        stop
      endif
      open(nfinp,file=filen,iostat=ios,status='old')
      read(nfinp,*) idummy
      read(nfinp,*) dummy, dummy, ntyp, natm,natm2
      ktyp=ntyp
      katm=natm2
      allocate(cpsf(3,katm),posf(3,katm))
      read(nfinp,*) idummy, idummy
      read(nfinp,'(a)')string
      string=adjustl(string)
      if(string(1:9).eq.'Cartesian'.or.&
         string(1:9).eq.'cartesian'.or.&
         string(1:9).eq.'CARTESIAN'     )then
        do jj=1,3
          read(nfinp,*)(altv(ii,jj),ii=1,3)
        enddo        
      else
!       backspace(nfinp)
!       read(nfinp,*)aaa,bbb,ccc,alpha,beta,gamma
        read(string,*)aaa,bbb,ccc,alpha,beta,gamma
        altv(:,:)=0.d0
        alpha=alpha/180.d0*pi
        beta =beta /180.d0*pi
        gamma=gamma/180.d0*pi
        altv(1,1)=aaa
        altv(1,2)=bbb*cos(gamma)
        altv(2,2)=bbb*sin(gamma)
        tmp=(cos(alpha)-cos(beta)*cos(gamma))/sin(gamma)
        altv(1,3)=ccc*cos(beta)
        altv(2,3)=ccc*tmp
        altv(3,3)=sqrt(ccc**2-altv(1,3)**2+altv(2,3)**2)
      endif
      read(nfinp,'(a)')string
      string=adjustl(string)
      if(string(1:6).eq.'dipole'.or. &
         string(1:6).eq.'DIPOLE'.or. &
         string(1:6).eq.'Dipole')then
        read(nfinp,*) idummy, idummy, idummy
      else
        read(string,*) idummy, idummy, idummy
      endif
      read(nfinp,*) ncord,ninv
      if(ncord.eq.1)then
        do iatm=1,katm
          read(nfinp,*)(cpsf(ii,iatm),ii=1,3),&
                       iwei(iatm),imdtyp(iatm),ityp(iatm)
        enddo
      elseif(ncord.eq.0)then
        do iatm=1,katm
          read(nfinp,*)(posi(ii,iatm),ii=1,3),&
                       iwei(iatm),imdtyp(iatm),ityp(iatm)
        enddo
        do iatm=1,katm
          cpsf(1,iatm)=altv(1,1)*posi(1,iatm)&
                      +altv(1,2)*posi(2,iatm)&
                      +altv(1,3)*posi(3,iatm)
          cpsf(2,iatm)=altv(2,1)*posi(1,iatm)&
                      +altv(2,2)*posi(2,iatm)&
                      +altv(2,3)*posi(3,iatm)
          cpsf(3,iatm)=altv(3,1)*posi(1,iatm)&
                      +altv(3,2)*posi(2,iatm)&
                      +altv(3,3)*posi(3,iatm)
        enddo
      endif 
      close(nfinp)
!     ==--------------------------------------------------------------==
!     ==  output                                                      ==
!     ==--------------------------------------------------------------==
      spring=0.02d0
       nimage=ndiv+1
      do iimage=1,ndiv+1
         fac1=dble(ndiv-iimage+1)/dble(ndiv)
         fac2=dble(iimage-1)/dble(ndiv)
        do iatm=1,natm2
          cps(:,iatm)=cpsi(:,iatm)*fac1+cpsf(:,iatm)*fac2
        enddo
        write(c2,'(i2.2)')iimage
        filen='nudged_'//trim(c2)
        tmpfile='cps_'//trim(c2)
        open(nudged,file=filen,status='unknown')
        write(nudged,'(f18.8)')spring
        do iatm=1,natm2
          write(nudged,'(i5,3f20.12)')iatm,(cps(ii,iatm),ii=1,3)
        enddo
        close(nudged)
        write(*,'(a,i2.2,a,a)')'Image #',iimage,' ---> ',trim(filen)
        write(*,'(f8.4,f8.4)')fac1,fac2
        do iatm=1,natm2
          write(*,'(3f20.12,3i5)')&
          (cps(kk,iatm),kk=1,3),iwei(iatm),imdtyp(iatm),ityp(iatm)
        enddo
        open(31,file=tmpfile,status='unknown')
        do iatm=1,natm2
          write(31,'(3f20.12,3i5)')&
          (cps(kk,iatm),kk=1,3),iwei(iatm),imdtyp(iatm),ityp(iatm)
        enddo
        close(31)
      enddo
!     ==--------------------------------------------------------------==
      deallocate(cpsi,posi)
      deallocate(cpsf,posf)
      deallocate(cps)
      deallocate(iwei,imdtyp,ityp)
!     ==--------------------------------------------------------------==
      stop
      end
!     ==================================================================
